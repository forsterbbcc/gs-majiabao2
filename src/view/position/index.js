import React, {Component} from 'react';
import {Cache, Contracts, Data} from "../../module";
import SetSLSP from './config';
import {Header, Footer} from "../common";
import {Svg} from "../common";
import {Link} from 'react-router-dom';
import LoadingFunction from "../../lib/loading";

import AlertFunction from "../../lib/AlertFunction";
import {Req, Schedule} from "../../lib";
import empty from '../../images/Nf-svg/empty.svg';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rule: false,
            list: null,
            income: 0,
            config: false,
            target: null,
        }
    }

    render() {

        return (
            <div className={'position'}>
                {/* <Header title={'持仓'} right={'结算列表'} goTo={'/settlement'} {...this.props}/> */}
                <div className={'warp'}>
                    {
                        this.state.list === null ? '' : (
                            this.state.list.length === 0 ? (
                                <div className={'imgWrap'}>
                                    <Svg path={empty} className={'empty'}/>
                                    <p>暂时没有数据</p>
                                </div>
                            ) : (
                                <div className={'positionBox'}>
                                    <div className={'statistics'}>
                                        <div>
                                            <div className={'des'}>浮动总盈亏&nbsp;&nbsp;</div>
                                            <div className={this.state.income >= 0 ? ' money' : ' money'}>{this.state.income >= 0 ? `+${this.state.income }` : this.state.income}<span>元</span></div>
                                        </div>
                                        <div className={'btn'} onClick={() => this.closeAll()}>一键清仓</div>
                                    </div>
                                    <ul className={'flat'}>
                                        {
                                            this.state.list.map((item) => {
                                                return (
                                                    <li className={item.income >= 0 ? 'raiseLine' : 'fallLine'}>
                                                        <div>
                                                            <div>
                                                                <div className={'commodityWrapper'}>
                                                                    <div className={'commodity'}>{item.commodity} ({item.contract})</div>
                                                                    <div className={'commodity incomeStatus'+(item.income >= 0 ? ' raiseBackground' : ' fallBackground')}>{item.income >= 0 ? '涨' : '跌'}</div>
                                                                    <div className={'volume'}>x{item.volume}</div>
                                                                </div>
                                                                {/* <div className={item.income >= 0 ? 'raise' : 'fall'}>{item.income}</div> */}
                                                            </div>
                                                            <div className={'position-data'}>
                                                                <table border="1">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>止损 {item.stopLoss}</td>
                                                                            <td>买入 {item.opPrice.toFixed(item.priceDigit)}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>止盈 {item.stopProfit}</td>
                                                                            <td>买出 {item.cpPrice.toFixed(item.priceDigit)}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div className={item.income >= 0 ? 'price raise' : 'price fall'}>{item.income}</div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div className={'buttonWrapper'}>
                                                            <div className={'closeButtonBackground'} onClick={() => this.close(item.id)}>平仓</div>
                                                            <div className={'setProfitButtonBackground'} onClick={() => {this.setStopLoss(item.id)}}>设置止盈止损</div>
                                                        </div>
                                                    </li>
                                                )
                                            })
                                        }
                                    </ul>
                                </div>
                            )
                        )
                    }
                    <SetSLSP show={this.state.config} target={this.searchTarget(this.state.target)}
                             close={() => this.setState({config: false, target: null})} {...this.props}/>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.mounted = true;
        if (Contracts.initial) {
            Data.start('position');
        } else {
            Schedule.addEventListener('contractsInitial', () => {
                Data.start('position');
            }, this)
        }
        
        if (!!Cache.tradeList) {
            this._keepUpdate = true;
            this.updatePosition(true);
        } else {
            Schedule.addEventListener('getSchemeInfo', () => {
                this._keepUpdate = true;
                this.updatePosition(true);
            }, this)
        }
    }

    componentWillUnmount() {
        this.mounted = false;
        Data.end('position');
        Schedule.removeEventListeners(this);
        clearTimeout(this._keepUpdate);
        this._keepUpdate = null;
    }

    async close(id) {
        try {
            await Req({
                url: '/api/trade/close.htm',
                type: 'POST',
                data: {
                    bettingId: id,
                    tradeType: 2 ,
                    source: '下单'
                },
                animate: true
            });
            AlertFunction({
                title: '提示', msg: '卖出委托已提交', confirm: () => {
                    Cache.getUserInfo();
                }
            })
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg || err})
        }
    }


    async closeAll() {
        try {
            let t = this.state.list.map((o) => {
                return Req({
                    url: '/api/trade/close.htm',
                    type: 'POST',
                    data: {
                        bettingId: o.id,
                        tradeType: 2 ,
                        source: '下单'
                    },
                    ignore: true
                });
            });
            LoadingFunction();
            Promise.all(t).then((o) => {
                Schedule.dispatchEvent({event: 'loadingEnd'});
                let success = 0;
                let failure = 0;
                o.forEach((e) => {
                    if (e.success) {
                        success++;
                    } else {
                        failure++;
                    }
                });
                AlertFunction({title: '提示', msg: `平仓委托已提交,成功${success}单,失败${failure}单`});
                Cache.getUserInfo();
            });
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg || err});
        }
    }

    setStopLoss(id) {
        this.setState({
            config: true,
            target: id
        })
    }

    /**
     * 更新持仓
     */
    async updatePosition(init) {
        try {
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 1,
                    tradeType: 2,
                    beginTime: '',
                    _: new Date().getTime(),
                },
                animate: init
            });
            if (data) {
                this.dealPosition(data);
            }
        } catch (err) {
            AlertFunction({title: '错误', msg: err['errorMsg'] || err});
        } finally {
            if (this._keepUpdate !== null) {
                this._keepUpdate = setTimeout(() => this.updatePosition(), 1000);
            }
        }
    }

    /**
     * 更新持仓数据
     */
    dealPosition(data) {
        let quote, scheme;
        let income = 0;
        let position = data.map((e) => {
            quote = Data.total[e.contract];
            scheme = Cache.tradeList[e.contract];
            if (quote) {
                e.unit = scheme.priceUnit.mul(scheme.priceChange).mul(e.moneyType === 0 ? 1 : 0.1);
                e.current = Number(quote.price) || 0;
                if (!!quote.price && !!e.opPrice) {
                    if (e.isBuy) {
                        e.income = e.current.sub(e.opPrice).mul(e.volume).mul(scheme.priceUnit).mul(e.moneyType === 0 ? 1 : 0.1);
                    } else {
                        e.income = e.opPrice.sub(e.current).mul(e.volume).mul(scheme.priceUnit).mul(e.moneyType === 0 ? 1 : 0.1);
                    }
                    income = income.add(e.income);
                    return e;
                } else {
                    e.income = 0;
                    return e;
                }
            } else {
                return null;
            }
        });
        position.remove(null);
        if (position.findIndex((e) => e === null) === -1) {
            if (this.mounted) {
                this.setState({list: position, income: income});
            }
        }
    }

    searchTarget(id) {
        if (!!id) {
            return this.state.list.find((e) => e.id === id)
        } else {
            return {};
        }
    }
}