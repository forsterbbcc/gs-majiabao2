import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Header} from "../common";
import {Req} from "../../lib";
import {Data} from "../../module";
import {Svg} from "../common";
import plus from '../../images/Nf-svg/plus3.svg';
import reduce from '../../images/Nf-svg/reduce3.svg';
import AlertFunction from "../../lib/AlertFunction";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stopProfit: 1,
            stopLoss: 1,
            unit: 1
        };
        this.stopProfit = this.stopProfit.bind(this);
        this.addStopProfit = this.addStopProfit.bind(this);
        this.reduceStopProfit = this.reduceStopProfit.bind(this);
        this.stopLoss = this.stopLoss.bind(this);
        this.addStopLoss = this.addStopLoss.bind(this);
        this.reduceStopLoss = this.reduceStopLoss.bind(this);
        this.submit = this.submit.bind(this);
        
    }

    render() {
        console.log(this.props.target);
        return (
            <div className={`stopSPSL ${this.props.show ? 'come' : ''}`}>
                <Header title={'设置止盈止损'} back={() => {
                    this.props.close();
                    this.setState({
                        stopProfit: 1,
                        stopLoss: 1,
                        unit: 1
                    })
                }}/>
                <div className={'body'}>
                    {/* <table className={'information'}>
                        <tbody>
                        <tr>
                            <td>{this.props.target.commodity}</td>
                            <td className={`mid ${this.props.target.isBuy ? 'raise' : 'fall'}`}>买{this.props.target.isBuy ? '涨' : '跌 '}{this.props.target.volume}手</td>
                            <td className={`mid ${this.props.target.income >= 0 ? 'raise' : 'fall'}`}>{this.props.target.income}</td>
                        </tr>
                        <tr>
                            <td>
                                最新价{this.props.target.contract && Data.total[this.props.target.contract].price}
                            </td>
                            <td className={'mid'}>
                                {this.props.target.moneyType === 0 ? '标准' : '迷你'}
                            </td>
                            <td/>
                        </tr>
                        </tbody>
                    </table> */}
                    <div className={'infoWrapper'}>
                        <div className={'infoWrapper-top'}>
                            <div className={'titleWrapper'}>
                                <div className={'name'}>{this.props.target.commodity}</div>
                                <div className={'modelWrapper'}>
                                    {/* <div className={'model'}>{this.props.target.moneyType === 0 ? '标准' : '迷你'}</div> */}
                                    <div className={'model'}>({this.props.target.contract})</div>
                                </div>
                            </div>
                            <div className={'latestPrice'}>{this.props.target.income}</div>
                        </div>
                        <div className={'infoWrapper-bottom'}>
                            <div className={'classType'}>买{this.props.target.isBuy ? '涨' : '跌 '}&nbsp;&nbsp;{this.props.target.volume}手</div>
                            <div className={'latestPrice'}>最新价&nbsp;&nbsp;{this.props.target.contract && Data.total[this.props.target.contract].price}元</div>
                        </div>
                    </div>
                    <div className={'board'}>
                        <div className={'between'}>
                            <span>止盈金额</span>
                            <span>{this.props.target.unit && this.state.stopProfit.mul(this.props.target.unit).mul(this.props.target.volume)}</span>
                        </div>  
                        <div className={'range-controller'}>
                            <div className={'reduceWrap'} onClick={this.reduceStopProfit}>
                                <Svg path={reduce} className={'reduce'}/>
                            </div>
                            <input type="range" className={'input-range raise'} step={1} min={1}
                                   max={this.props.target.stopProfitBegin && (this.props.target.stopProfitBegin.div(this.state.unit))}
                                   onChange={this.stopProfit} value={this.state.stopProfit}/>
                            <div className={'plusWrap'} onClick={this.addStopProfit}>
                                <Svg path={plus} className={'plus'}/>
                            </div>
                        </div>
                        <div className={'between'}>
                            <span>止损金额</span>
                            <span>-{this.state.stopLoss.mul(this.state.unit)}</span>
                        </div>
                        <div className={'range-controller'}>
                            <div className={'reduceWrap'} onClick={this.reduceStopLoss}>
                                <Svg path={reduce} className={'reduce'}/>
                            </div>
                            <input type="range" className={'input-range fall'} step={1} min={1}
                                   max={this.props.target.stopLossBegin && (Math.abs(this.props.target.stopLossBegin).div(this.state.unit))}
                                   onChange={this.stopLoss} value={this.state.stopLoss}/>
                            <div className={'plusWrap'} onClick={this.addStopLoss}>
                                <Svg path={plus} className={'plus'}/>
                            </div>
                        </div>
                        
                        <div className={'submit'} onClick={this.submit}>
                            确定
                        </div>
                    </div>
                </div>
                {/* <div className={'remark'}>
                    <h4> 温馨提示</h4>
                    <ul>
                        <li>止盈金额：当盈利多于等于该金额之后，系统自动平仓</li>
                        <li>止损金额：当亏损多于等于该金额之后，系统自动平仓</li>
                        <li>如需帮助，请 <Link to={'/cs'}>联系客服</Link></li>
                    </ul>
                </div> */}
            </div>
        )
    }

    stopProfit(e) {
        this.setState({
            stopProfit: e.currentTarget.valueAsNumber
        })
    }

    addStopProfit() {
        const max = this.props.target.stopProfitBegin && (this.props.target.stopProfitBegin.div(this.state.unit));
        this.setState({
            stopProfit: Math.min(this.state.stopProfit + 1, max)
        })
    }

    reduceStopProfit() {
        this.setState({
            stopProfit: Math.max(this.state.stopProfit - 1, 1)
        })
    }

    stopLoss(e) {
        this.setState({
            stopLoss: e.currentTarget.valueAsNumber
        })
    }

    addStopLoss() {
        const max = this.props.target.stopLossBegin && (Math.abs(this.props.target.stopLossBegin).div(this.state.unit));
        this.setState({
            stopLoss: Math.min(this.state.stopLoss + 1, max)
        })
    }

    reduceStopLoss(){
        this.setState({
            stopLoss: Math.max(this.state.stopLoss - 1, 1)
        })
    }

    async submit() {
        try {
            await Req({
                url: '/api/trade/spsl.htm',
                type: 'POST',
                data: {
                    bettingId: this.props.target.id,
                    tradeType: 2,//this.props.location.state.simulate ? 2 : 1,
                    stopProfit: this.state.stopProfit.mul(this.state.unit),
                    stopLoss: -this.state.stopLoss.mul(this.state.unit),
                    source: '设置止盈止损'
                },
                animate: true
            });
            AlertFunction({title: '提示', msg: '设置止盈止损成功'});
            this.props.close();
            this.setState({
                stopProfit: 1,
                stopLoss: 1,
                unit: 1
            })
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg || err});
        }
    }

    componentWillReceiveProps({show, target}) {
        if (this.props.show !== show) {
            if (show === true) {
                const n = target.unit.mul(target.volume);
                this.setState({
                    unit: n,
                    stopProfit: target.stopProfit.div(n),
                    stopLoss: Math.abs(target.stopLoss).div(n)
                })
            }
        }
    }
}
