import React, {Component} from 'react';
import {Schedule} from "../../lib";

export default class App extends Component {
    constructor(props) {
        super(props);
        Schedule.addEventListener('loadingEnd',this.guess,this);
    }

    render() {
        return (
            <div className={'loading'}>
                <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                    <circle className="path" fill="none" strokeWidth="6" strokeLinecap="round" cx="33" cy="33" r="30" />
                </svg>
            </div>
        )
    }

    guess(){
        Schedule.removeEventListeners(this);
        this.props.close();
    }
}