import React, {Component} from 'react';
import {NavLink, Link} from 'react-router-dom';
import ReactSvg from 'react-svg';
import {Svg} from ".";


import trend from '../../images/Nf-svg/trend.svg';
import tv from '../../images/Nf-svg/tv.svg';
import home from '../../images/Nf-svg/home.svg';
import mine from '../../images/xlt/mine.svg';
import main from './../../images/xlt/main.svg';
import trade from './../../images/xlt/trade.svg';
import notice from './../../images/xlt/info.svg';
import account from './../../images/mdf-svg/account.png';

import {Contracts} from "../../module";
import {Schedule} from "../../lib";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contract: null
        };
        if (Contracts.initial) {
            let [o] = Contracts.foreignArray;
            this.state.contract = o.contract
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }

    render() {
        return (
            <footer>
                <NavLink to={'/'} activeClassName={'footActive'}
                         isActive={(match, location) => location.pathname === '/'}>
                    <Svg path={main} className={'iconStyle home'}/>
                    <span className={'text'}>首页</span>
                </NavLink>
                <NavLink to={{pathname:'/quotation',state:{simulate:true, type:1}}} activeClassName={'footActive'}>
                    <Svg path={trade} className={'iconStyle trend'}/>
                    <span className={'text'}>行情</span>
                </NavLink>
                <NavLink to={{pathname:'/live/finance'}} activeClassName={'footActive'}>
                    <Svg path={notice} className={'iconStyle tv'}/>
                    <span className={'text'}>资讯</span>
                </NavLink>
                <NavLink to={'/accountSet'} activeClassName={'footActive'}>
                    <Svg path={mine} className={'iconStyle mine'}/>
                    <span className={'text'}>我的</span>
                </NavLink>
            </footer>
        )
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }

    updateContracts() {
        let [o] = Contracts.foreignArray;
        this.setState({
            contract: o.contract
        })
    }
}