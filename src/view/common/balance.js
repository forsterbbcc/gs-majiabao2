import React, {Component} from 'react';
import {Cache} from "../../module";
import {Schedule} from "../../lib";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            balance:Cache.realBalance
        }
    }

    render() {
        return (
            <div className={'balance'}>
                <span>账户余额</span>
                <b>{this.state.balance.toFixed(2)}<span>元</span></b>
            </div>
        )
    }

    componentDidMount(){
        if(Cache.userId !== ''){
            this.setState({
                balance:Cache.realBalance
            })
        }else{
            Schedule.addEventListener('getUserInfo',()=>{
                this.setState({
                    balance:Cache.realBalance
                })
            },this)
        }
    }

    componentWillUnmount(){
        Schedule.removeEventListeners(this)
    }

}