import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Req, Schedule} from "../../lib";
import {Svg, Footer} from "../common";
import {Contracts, Data} from "../../module";
import {Cache} from "../../module";
import content from './../../lib/content.json';
import ImageBanner from './../home/imageBanner'
import HomeNotice from './../home/homeNotices';
import moment from 'moment-timezone';
//todo 图片引入

import menu from './../../images/qh/menu.svg';
import cs from './../../images/qh/cs.svg';

import quotation from './../../images/qh/home-quotation.svg';
import trade from './../../images/qh/home-trade.svg';
import position from './../../images/qh/home-position.svg';
import favorite from './../../images/qh/home-favorite.svg';
import information from './../../images/qh/home-information.svg';
import event from './../../images/qh/home-event.svg';
import AlertFunction from '../../lib/AlertFunction';


export default class App extends Component {
    searchAry = null;

    constructor(props) {
        super(props);
        this.state = {
            notices: [],
            isLogin: Cache.isLogin(),
            foreignArray: [],
            stockArray: [],
            domesticArray: [],
            searchInfo: '',
            searchShow: false,
            allData:[],
            contract:null,
            timeStamp:Date.now(),
            page:'foreign'
        };

        if (Contracts.initial) {
            this.state.foreignArray = Data.foreignBrief;
            this.state.stockArray = Data.stockBrief;
            this.state.domesticArray = Data.domesticBrief;
            this.state.allData = this.state.allData.concat(Data.foreignBrief)
            this.state.allData = this.state.allData.concat(Data.stockBrief)
            this.state.allData = this.state.allData.concat(Data.domesticBrief)
            let [o] = Contracts.foreignArray;
            this.state.contract = o.contract;
            this.state.hot = Contracts.hot;
            this.state.news = Contracts.new;
            Data.start('updateBrief');
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
        // console.log(this.state,'this.state');

    }

    // renderQuotation(){
    //     return this.state.foreignArray.map((item,i)=>{
    //         return(
    //             <Link to={{pathname:'/trade',state:{contract:item.code}}}>
    //                 <div className={'titleWrapper'}>
    //                     <div>{item.name}</div>
    //                     <div>{item.code}</div>
    //                 </div>
    //                 <div>{item.price}</div>
    //                 <div className={'imageWrapper'}>
    //                     <div className={'downLine'}>
    //                         <Svg path={item.isUp ? upLine : downLine}/>
    //                     </div>
    //                     <Svg path={item.isUp ? upQuota : downQuota}/>
    //                     <div className={item.isUp?'rate red':'rate green'}>{item.rate}</div>
    //                 </div>
    //             </Link>
    //         );
    //     })
    // }

    renderArticle(){
        return content.content.map((item,i)=>{
            return(
                <li>
                    <div className={'keyWordBox'}>
                        <div>{item.keyWord}</div>
                    </div>
                    <div className={'titleBox'}>
                        <div>{item.title}</div>
                    </div>
                </li>
            );
        })
    }

    renderCategoryBox(path,title,colorClass,to){
        if (title === '登出') {
            return(
                <a onClick={()=>AlertFunction({title: '警告', msg: '是否退出账号？', hasCancel: true, confirm: () => Cache.logout()})} className={`colorBox ${colorClass}`}>
                    <div className={'imageBox'}>
                        <Svg path={path}/>
                    </div>
                    <div className={'title'}>{title}</div>
                </a>
            );
        }else if (title === '分享') {
            return(
                <a onClick={()=>{
                    Schedule.dispatchEvent({event:'openShare'});
                }} className={`colorBox ${colorClass}`}>
                    <div className={'imageBox'}>
                        <Svg path={path}/>
                    </div>
                    <div className={'title'}>{title}</div>
                </a>
            );
        }else{
            return(
                <Link to={to||'/'} className={`colorBox ${colorClass}`}>
                    <div className={'imageBox'}>
                        <Svg path={path}/>
                    </div>
                    <div className={'title'}>{title}</div>
                </Link>
            );
        }
    }

    render() {
        // console.log(this.state,'this.state');

        return (
            <div className={'home'}>
                
                <div className={'m-header'}>
                    <div className={'imageBox menuButton'} onClick={()=>Schedule.dispatchEvent({event:'openDrawer'})}>
                        <Svg path={menu}/>
                    </div>
                    <div className={'title'}>主页</div>
                    <Link to={'/cs'} className={'kefu'}>
                        <Svg path={cs}/>
                    </Link>
                </div>
                <ImageBanner/>
                <table className={'globalTimeBox'}>
                    <tbody>
                        <tr>
                            <td className={'beijing'}>
                                <div className={'dateAndTimeBox'}>
                                    <div>北京时间 </div>
                                    <div>{moment(this.state.timeStamp).tz('Asia/Shanghai').format('YYYY-MM-DD HH:mm:ss ')}</div>
                                </div>
                            </td>
                            <td className={'london'}>
                                <div className={'dateAndTimeBox'}>
                                    <div>伦敦时间</div>
                                    <div>{moment(this.state.timeStamp).tz('Europe/Guernsey').format('YYYY-MM-DD HH:mm:ss ')}</div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td className={'newYork'}>
                                <div className={'dateAndTimeBox'}>
                                    <div>纽约时间</div>
                                    <div>{moment(this.state.timeStamp).tz('America/New_York').format('YYYY-MM-DD HH:mm:ss ')}</div>
                                </div>
                            </td>
                            <td className={'tokyo'}>
                                <div className={'dateAndTimeBox'}>
                                    <div>东京时间</div>
                                    <div>{moment(this.state.timeStamp).tz('Asia/Tokyo').format('YYYY-MM-DD HH:mm:ss ')}</div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <HomeNotice/>

                <div className="quotation">
                        <Position title={'国际期货'}
                            page={this.state.page}
                            array={()=>{
                                if (this.state.page === 'foreign') {
                                    return this.state.foreignArray
                                }else if (this.state.page === 'domestic') {
                                    return this.state.domesticArray
                                }else if (this.state.page === 'stock') {
                                    return this.state.stockArray
                                }
                            }}
                            hot={this.state.hot}
                            new={this.state.news}
                            to={'foreign'}
                            selectPage={(value)=>{this.setState({page:value})}}
                        />
                </div>

                <table className={'homeCategory'}>
                    <tbody>
                        <tr>
                            <td>{this.renderCategoryBox(information,'最新资讯','quotation-color',{pathname:'/live/lives',state:{simulate:true}})}</td>
                            <td>{this.renderCategoryBox(event,'分析师专栏','trade-color',{pathname:'/column',state:{simulate:true,contract:this.state.contract}})}</td>
                        </tr>
                        <tr>
                            <td>{this.renderCategoryBox(quotation,'行情','quotation-color',{pathname:'/quotation',state:{simulate:true}})}</td>
                            <td>{this.renderCategoryBox(trade,'模拟','trade-color',{pathname:'/trade',state:{simulate:true,contract:this.state.contract}})}</td>
                        </tr>
                        <tr>
                        <td>{this.renderCategoryBox(position,'持仓','position-color',{pathname:'/position',state:{page:'position'}})}</td>
                            <td>{this.renderCategoryBox(favorite,'我的自选','favorite-color',{pathname:'/quotation',state:{simulate:true,page:'custom'}})}</td>
                        </tr>
                        {/* <tr>
                            <td>{this.renderCategoryBox(notice,'公告','notice-color','/notice')}</td>
                            <td>{this.renderCategoryBox(position,'持仓','position-color',{pathname:'/position',state:{page:'position'}})}</td>
                        </tr> */}
                        {/* <tr>
                            <td>{this.renderCategoryBox(share,'分享','share-color')}</td>
                            <td>{this.renderCategoryBox(this.state.isLogin?logout:login,this.state.isLogin?'登出':'登录','logout-color','/login')}</td>
                        </tr> */}
                    </tbody>
                </table>
            </div>
        )
    }

    componentDidMount() {
        Schedule.addEventListener('updateBrief', this.updateBrief, this)
        this.updateTime();
        if (this.state.isLogin) {
            this.loginCallback()
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this);
        }
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
        Data.end('updateBrief');
    }

    updateTime(){
        setTimeout(() => {
            this.setState({
                timeStamp:Date.now()
            },()=>this.updateTime());
        }, 500);
    }

    updateContracts() {
        let [o] = Contracts.foreignArray;
        let temp = [];
        temp = temp.concat(Data.foreignBrief)
        temp = temp.concat(Data.stockBrief)
        temp = temp.concat(Data.domesticBrief)
        this.setState({
            contract: o.contract,
            foreignArray: Data.foreignBrief,
            stockArray: Data.stockBrief,
            domesticArray: Data.domesticBrief,
            hot: Contracts.hot,
            allData:temp
        });
        Data.start('updateBrief');
    }

    updateBrief() {
        this.setState({
            foreignArray: Data.foreignBrief,
            stockArray: Data.stockBrief,
            domesticArray: Data.domesticBrief
        });
    }

    //todo 搜索
    search(info) {
        if (!info) {
            return [];
        } else {
            //todo 如果以字母与数字搜索
           if((/^[A-Za-z0-9]+$/).test(info)){
               info = info.toUpperCase();
               return Contracts.totalArray.filter((item) => {
                   return item.contract.startsWith(info)
               });
           }else if(/^[\u4e00-\u9fa5]{0,}$/){ //todo 如果以汉字搜索
               return Contracts.totalArray.filter((item) => {
                   return item.name.startsWith(info)
               });
           }
        }
    }

    // generatorBannerData(array){
    //     let temp = [];
    //     for (let i = 0; i < array.length; i+=2) {
    //         const element1 = array[i];
    //         let obj = [
    //             element1,
    //             array[i+1] || 'empty'
    //         ]
    //         temp.push(obj);
    //     }
    //     return temp;
    // }

    loginCallback(){
        this.setState({
            isLogin:Cache.isLogin()
        })
    }
}

class Position extends Component {
    constructor(props) {
        super(props);

        // this.getAppearence().then(data=>console.log(data)).catch(e=>console.log(e))
    }

    render() {
        if (this.props.array().length !== 0) {
            return (
                <div className={'typeBoxH'}>
                    <div className={'titleBox'}>
                        <div onClick={()=>this.props.selectPage('foreign')} className={`title ${this.props.page === 'foreign'?'active':''}`}>{'国际期货'}</div>
                        <div onClick={()=>this.props.selectPage('stock')} className={`title ${this.props.page === 'stock'?'active':''}`}>{'股指期货'}</div>
                        <div onClick={()=>this.props.selectPage('domestic')} className={`title ${this.props.page === 'domestic'?'active':''}`}>{'国内期货'}</div>
                        {/* <Link to={{pathname:'/quotation',state:{page:this.props.to}}} className={'imageBox'}>
                            <Svg path={back}/>
                        </Link> */}
                    </div>
                    <ul>
                        {
                            this.props.array().map((item,i) => {
                                if (i>2){
                                    return null;
                                }
                                return (
                                    <li key={i} className={item.isOpen ? (item.isUp ? 'bgred' : 'bggreen') : null}>
                                        <Link to={{
                                            pathname: '/trade',
                                            state: {simulate: false, contract: item.code, goodsCode: item.goodsCode , name: item.name}
                                        }}>
                                            <div className={'top'}>
                                                {item.name}
                                                {
                                                    Contracts.isHot(item.code) ? (
                                                        <span className={'red'}>HOT</span>
                                                    ) : (null)
                                                }
                                                {
                                                    Contracts.isNew(item.code) ? (
                                                        <span className={'green'}>NEW</span>
                                                    ) : (null)
                                                }
                                            </div>
                                            <div className={item.isUp ? 'red middle' : 'green middle'}>
                                                {item.price || '- -'}
                                            </div>
                                            <div
                                                className={`bottom ${item.isOpen ? (item.isUp ? 'red' : 'green') : 'rest'}`}>
                                                {
                                                    item.isOpen ? item.rate : '休市'
                                                }
                                            </div>
                                        </Link>
                                    </li>
                                )
                            })
                        }
                    </ul>
                    {/* <ReactSwipe className="selfQuotation"
                                swipeOptions={{continuous: false, speed: 500, startSlide: 0, auto: false}}
                                key={this.props.array.length}>
                                {this.renderCell()}
                    </ReactSwipe> */}
                </div>
            )
        }else{
            return(
                ''
            );
        }
    }
}
