import React, {Component} from 'react';
import {Header} from "../common";
import {Cache} from "../../module";
import {Schedule} from "../../lib";
import CopyToClipboard from 'react-copy-to-clipboard'

import bg from './../../images/xlt/recommend-bg.jpg'


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            isLogin:Cache.isLogin(),
            userId:'',
            qrAddress:'',
            successful: false,
            linkAddress:''
        }
    }

    render() {
        return (
            <div className={'recommend'}>
                <Header title={'推荐好友'} {...this.props}/>
                <div className={'main'}>
                    <img src={bg} alt=""/>
                    <img className={'p'} src={this.state.qrAddress} />
                    <div className={'shareBox'}>
                        <div className={'shareLeft'}>
                            <p>分享您的专属链接</p>
                            <div className={'shareAddress'}>{this.state.linkAddress}</div>
                        </div>
                        <div className={'shareRight'}>
                            <CopyToClipboard  text={this.state.linkAddress} onCopy={()=>this.onCopy()}>
                                <div>复制</div>
                            </CopyToClipboard>
                        </div>
                    </div>
                </div>
                {
                    this.state.successful?(
                        <div className={'success'}>
                            <div>复制成功，快去推广吧！</div>
                        </div>
                    ):(null)
                }
            </div>
        )
    }

    componentDidMount(){
        if(Cache.initial){
            this.loginCallback()
        }else{
            Schedule.addEventListener('cacheInitial', this.loginCallback, this)
        }
        if(Cache.userId !== ''){
            this.getInfoCallback();
        }else{
            Schedule.addEventListener('getUserInfo', this.getInfoCallback, this)
        }

        this.setState({
            qrAddress: `/api/vf/tdCode.gif?type=tj&url=${window.location.origin}/?ru=${this.state.userId}`
        })
    }

    componentWillUnmount(){
        Schedule.removeEventListeners(this)
    }

    loginCallback(){
        this.setState({
            isLogin:Cache.isLogin()
        })
    }

    getInfoCallback(){
        this.setState({
            userId:Cache.userId,
            linkAddress:`${window.location.origin}/?ru=${Cache.userId}`
        })
    }

    //todo 复制
    onCopy(){
        this.setState({successful:true});

        setTimeout(()=>{
            this.setState({successful:false})
        },1500)
    }
}
