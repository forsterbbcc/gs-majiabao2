import React, {Component} from 'react';
import ReactSwipes from 'react-swipes';
import {Link} from 'react-router-dom';
import {Store} from "../../module/index";

import down from './../../images/mdf-svg/down.svg'
import up from './../../images/mdf-svg/up.svg'
import { Svg } from '../common';
export default class App extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            banners: [],
            selectedIndex:0,
            curCard: ''
        }
    }

    renderSubItem(index){
        let data = [];
        if (index === 0) {
            data = this.props.foreign;
        }else if (index === 1) {
            data = this.props.domestic;
        }else if (index === 2) {
            data = this.props.stock;
        }
        let content = data.map((element,i)=>{
            if (i >2) {
                return '';
            }
            
            for (let index = data.length; index < 3; index++) {
                data.push('empty');
            }

            if (element === 'empty') {
                return(
                    <Link className={'unitButton'} to={'/'}>
                        <div className={'hide'}>{element.price || '-'}</div>
                        <div className={''}>{'敬请期待'}</div>
                        <div className={'hide'}>{element.name}</div>
                    </Link>
                );
            }else{
                return(
                    <Link className={'unitButton'} to={{
                        pathname: '/trade',
                        state: {simulate: this.props.simulate, code: element.code, name: element.name, goodsCode:element.goodsCode}
                    }}>
                        <div className={element.isUp?'raise price':'fall price'}>{element.price || '-'}</div>
                        <div className={element.isUp?'raise rate':'fall rate'}>{`(${element.rate || '-'})` || '-'}</div>
                        <div>{element.name}</div>
                    </Link>
                );
            }
        })
        return content
    }

    renderItems(){
        let category = [0,1,2]
        
        return category.map((item,i)=>{
            return (
                <div>
                    <div className={'itemBox item'}>
                        {/* <Link to={'/'}>
                            <div>{item.price}</div>
                            <div>{item.rate}</div>
                            <div>{item.name}</div>
                        </Link> */}
                        {this.renderSubItem(item)}
                    </div>
                </div>
            )
        });
    }

    renderBannerButton(){
        return(
            <ul>
                <li onClick={()=>this.selectButton(0)} className={this.state.selectedIndex === 0 ? 'bannerButtonActive' : ''}>国内热门</li>
                <li onClick={()=>this.selectButton(1)} className={this.state.selectedIndex === 1 ? 'bannerButtonActive' : ''}>国际热门</li>
                <li onClick={()=>this.selectButton(2)} className={this.state.selectedIndex === 2 ? 'bannerButtonActive' : ''}>股指热门</li>
            </ul>
        );
    }

    render() {
        return (
            
            <div className={'bannerBox card-swipe'}>
                {this.renderBannerButton()}
                <ReactSwipes className={"card-slide"} options={{
                    distance: 340, // 每次移动的距离，卡片的真实宽度
                    currentPoint: 0,// 初始位置，默认从0即第一个元素开始
                    autoPlay: false, // 是否开启自动播放
                    swTouchstart: (ev) => {
                    },
                    swTouchmove: (ev) => {
                    },
                    swTouchend: (ev) => {
                        let data = {
                            moved: ev.moved,
                            originalPoint: ev.originalPoint,
                            newPoint: ev.newPoint,
                            cancelled: ev.cancelled,
                        };
                        this.setState({selectedIndex: data.newPoint});
                    }
                }} ref={(e)=>this._swiper=e}>
                    {this.renderItems()}
                </ReactSwipes>
            </div>
        )

    }

    componentDidMount() {
        this.mounted = true;
        Store.banner.get().then((data) => {
            if (this.mounted) {
                this.setState({
                    banners: data
                })
            }
        })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    selectButton(value){
        this.setState({
            selectedIndex:value
        });
        // this.view.slide(value,500);
        this._swiper.swipes.moveToPoint(value,500);
    }
}