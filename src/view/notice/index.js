import React, {Component} from 'react';
import {Header, Drawer, Svg} from "../common";
import {Store} from "../../module";
import {Link} from "react-router-dom"
import {formatDate} from "../../lib/tool";

import back from '../../images/Nf-svg/back.svg';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    renderItem(){
        return this.state.data.map((item, key) => {
             if (item.isShow) {
                 return (
                     <div className={'infoBox'} onClick={()=>this.show(key)}>
                         <div>
                             <div className={'infoMain infoMain-column'}>
                                 <div className={'infoMain-top'}>
                                     <div className={'title'}>{item.title}</div>
                                     <div className={'blueDot blueDot-up'}></div>
                                 </div>
                                 <div className={'infoMain-bottom'}>
                                     <div className={'contentWrapper'}>
                                         <div className={'content'}>{item.content}</div>
                                         {/* <div className={'time'}>{formatDate('y-m-d h:i:s', {date: item.time.time})}</div> */}
                                     </div>
                                 </div>
                             </div>
                         </div>
                         
                     </div>
                 )
             }else{
                 return (
                     <Link to={{pathname:'/noticeInfo',state:item}} className={'infoBox'} /*onClick={()=>this.show(key)}*/>
                         <div>
                             <div className={'infoMain'}>
                                 <div className={'title'}>{item.title}</div>
                                 <div className={'blueDot blueDot-down'}></div>
                                 {/* <div className={'contentWrapper'}>
                                     <div className={'content'}>{item.content}</div>
                                      <div className={'blueDot'}></div>
                                 </div> */}
                                 {/* <div className={'time'}>{formatDate('y-m-d h:i:s', {date: item.time.time})}</div> */}
                             </div>
                         </div>
                     </Link>
                 )
             }
         })
     }
 
 

    render() {
        return (
            <div className={'noticeBox'}>
                <Header title={'公告'} {...this.props}/>
                <div className={'notices'}>
                    {this.renderItem()}
                </div>
            </div>
        )
    }

    componentDidMount() {
        (
            async () => {
                try {
                    let result = await Store.notice.get();
                    for (let i = 0; i < result.notices.length; i++) {
                        const element = result.notices[i];

                        element.content = element.content.replace(new RegExp('<p>','g'),'');
                        element.content = element.content.replace(new RegExp('</p>','g'),'');
                        element.content = element.content.replace(new RegExp('<b>','g'),'');
                        element.content = element.content.replace(new RegExp('</b>','g'),'');
                        element.content = element.content.replace(new RegExp('</br>','g'),'');
                        element.content = element.content.replace(new RegExp(' ','g'),'');

                        let date = new Date(element.time.time);
                        let dateString = formatDate('y-m-d h:i:s', {date});
                        element.dateString = dateString;
                    }
                    console.log(result.notices);
                    this.setState({data: result.notices});
                } catch (e) {

                }
            }
        )();
    }

    show(index){
        let temp = [].concat(this.state.data);
        for (let i = 0; i < this.state.data.length; i++) {
            const element = this.state.data[i];
            if (i === index) {
                element.isShow = !element.isShow;
            } 
            // temp.push(element);
        }
        this.setState({
            data:temp
        });
    }

}