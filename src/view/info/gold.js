import React, {Component} from 'react'
import ReactPullLoad, {STATS} from 'react-pullload'
import {Link} from 'react-router-dom'
import {Req} from "../../lib/index";
import '../../../node_modules/react-pullload/dist/ReactPullLoad.css'

let hasMore = true;
export default class App extends Component {

  mount = true;

  constructor(props) {
    super(props);
    this.state = {
      hasMore: true,
      data: [],
      action: STATS.init,
    }
  }

  componentDidMount() {
    // this.update(1);
    this.handRefreshing();
  }

  componentWillUnmount(){
        this.mount = false
    }

  async update(type, date) {
    try {
      const result = await Req({
        url: '/api/news/newsList.htm',
        data: {
          type: type,
          date: date
        }
      })
      if (result && result.code && result.code == 200) {
        if (date) {

          if (this.mount === false){
            return;
          }

          this.setState({
            data: this
              .state
              .data
              .concat(result.newsList),
            action: STATS.reset,
          });
        } else {

            if (this.mount === false){
                return;
            }

            if (STATS.refreshing == this.state.action) {
            this.setState({data: result.newsList, action: STATS.refreshed});
          } else {
            this.setState({data: result.newsList});
          }
        }
      }
    } catch (err) {
    }
  }

  handleAction = (action) => {
    //new action must do not equel to old action
    if (action === this.state.action) {
      return false
    }

    if (action === STATS.refreshing) {
      this.handRefreshing();
    } else if (action === STATS.loading) {
      this.handLoadMore();
    } else {
      //DO NOT modify below code
        if (this.mount === false) {
            return;
        }
      this.setState({action: action})
    }
  }

  handRefreshing = () => {
    if (STATS.refreshing === this.state.action) {
      return false
    }

    this.update(1)

      if (this.mount === false){
          return;
      }

      this.setState({action: STATS.refreshing})
  }

  handLoadMore = () => {
    if (STATS.loading === this.state.action) {
      return false
    }
    //无更多内容则不执行后面逻辑
    if (!this.state.hasMore) {
      return;
    }

    let dataLength = this.state.data.length;
    let lastDate = this.state.data[dataLength - 1].date;
    this.update(1, lastDate)

      if (this.mount === false){
          return;
      }

      this.setState({action: STATS.loading})
  }

  renderItem() {
    let content = this
      .state
      .data
      .map((item, i) => {
        return (
          <tr key={i}>
            <td>
              <Link to={'/detail/' + item.id}>
                <div>
                  <h2>{item.title}</h2>
                  <p>{item.date}</p>
                </div>
                <div>
                  <img src={item.thumb}/>
                </div>
              </Link>
            </td>
          </tr>
        );
      })
    return content;
  }

  render() {
    return (

      <div>

        <ReactPullLoad
          downEnough={50}
          action={this.state.action}
          handleAction={this.handleAction}
          hasMore={hasMore}
          style={{
          backgroundColor: "#f4f4f4",
              paddingTop:89
        }}
          distanceBottom={1000}>
          <table className='gold'>
            <tbody>
              {this.renderItem()}
            </tbody>
          </table>
        </ReactPullLoad>
      </div>

    );
  }
}
