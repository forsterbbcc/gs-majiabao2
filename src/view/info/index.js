import React, {Component} from 'react'
import Footer from './../common/footer'
import RouteWithSubRoutes from './../../routes/routeWithSubRoutes'
import {NavLink,Link} from 'react-router-dom'

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isShowDetail:false
        }
    }

    render() {
        const a = this.props.routes;
        return (
            <div className='info'>
                <div>资讯</div>
                <ul>
                    <NavLink to={'/news/live'} activeClassName={'selected'}>资讯直播</NavLink>
                    <NavLink to={'/news/gold'} activeClassName={'selected'}>原油  金银</NavLink>
                    {/*<NavLink to={'/news/gold'} activeClassName={'selected'}></NavLink>*/}
                </ul>
                {a.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
                <div>
                    <Footer/>
                </div>
            </div>        
        );
    }
}
