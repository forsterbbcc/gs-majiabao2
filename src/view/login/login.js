import React, {Component} from 'react';
import {Redirect, Link} from 'react-router-dom';
import {Cache} from "../../module";
import {Svg, Header} from "../common";
import AlertFunction from "../../lib/AlertFunction";

//todo 引入图片

// import loginLogo from '../../images/mdf-svg/login-logo.svg';
import close from '../../images/Nf-svg/closeEye.svg';
import loginLogo from './../../images/qh/login-logo.png'
import loginID from './../../images/qh/login-id.svg'
import loginPassword from './../../images/qh/login-password.svg'

export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            redirectToReferrer: false,
            username: '',
            password: '',
            open: false
        };
    }

    render() {
        const {from} = this.props.location.state || {from: {pathname: '/'}};
        const {redirectToReferrer} = this.state;
        if (redirectToReferrer) {
            return <Redirect to={from}/>
        }
        return (
            <div className={'login'}>
                

                {/* <h4>欢迎登录</h4> */}
                {/* <div className={'loginLogoWrapper'}>
                    <Svg path={loginLogo}/>
                </div> */}
                <Header {...this.props} title={'登录'}/>
                
                <div className={'main'}>
                    <div className={'logoWrapper'}>
                        <div className={'logoBox'}>
                            <img alt={''} src={loginLogo}/>
                        </div>
                    </div>
                    <div className={'loginInfo'}>
                        <div className={'passwordBox'}>
                            {/* <div className={'passwordBox-title'}>手机号</div> */}
                            <div>
                                <Svg path={loginID}/>
                            </div>
                            <input type="tel" placeholder={'请输入手机号'} value={this.state.username} maxLength={11}
                                   onChange={(e) => {
                                       this.setState({username: e.target.value})
                                   }}/>
                        </div>
                    </div>
                    <div className={'loginInfo'}>
                        {
                            this.state.open ? (
                                <div className={'passwordBox'}>
                                    {/* <div className={'passwordBox-title'}>密码</div> */}
                                    <div>
                                        <Svg path={loginPassword}/>
                                    </div>
                                    <input type="text" placeholder={'字母和数字组合'} value={this.state.password}
                                           onChange={(e) => {
                                               this.setState({password: e.target.value})
                                           }}/>
                                </div>
                            ) : (
                                <div className={'passwordBox'}>
                                    {/* <div className={'passwordBox-title'}>密码</div> */}
                                    <div>
                                        <Svg path={loginPassword}/>
                                    </div>
                                    <input type="password" placeholder={'字母和数字组合'} value={this.state.password}
                                           onChange={(e) => {
                                               this.setState({password: e.target.value})
                                           }}/>
                                </div>
                            )
                        }
                    </div>
                    <div className={'buttonBox'} onClick={() => this.setLogin()}>
                        <div className={'button'}>登录</div>
                    </div>
                    <div className={'none'}>
                        <Link to={{pathname: '/register'}} className={'registerButton'}>注册</Link>
                    </div>
                    {/* <div className={'last'}>
                        <Link to={{pathname: '/recoverPassword'}} className={'forgot'}>忘记密码？</Link>
                    </div> */}
                </div>
            </div>
        )
    }


    setLogin() {
        if (this.state.username.length === 0) return AlertFunction({title: '警告', msg: '请输入手机号'});
        if (this.state.username.length !== 11) return AlertFunction({title: '警告', msg: '请输入正确的手机号'});
        if (this.state.password.length === 0) return AlertFunction({title: '警告', msg: '请输入密码'});

        const {from} = this.props.location.state || {from: null};
        Cache.setLogin(this.state.username, this.state.password, () => {
            if (from === null) {
                window.history.back();
            } else {
                this.setState({redirectToReferrer: true})
            }
        })
    }

}