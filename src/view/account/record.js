import React,{Component} from 'react'
import { Header, Svg } from '../common';
import {Link} from 'react-router-dom'
import love from './../../images/xlt/love.svg';
import AlertFunction from '../../lib/AlertFunction';

import empty from './../../images/xlt/_empty search.svg'
export default class App extends Component{
    constructor(props){
        super(props);

        this.state = {
            data:JSON.parse(localStorage.getItem('record')) || [],
            edit:false,
            deleteIDs:[]
        }
    }

    renderEditableButton(){
        if (this.state.edit) {
            return(
                <div className={'editableButton'}>
                    <ul>
                        <li onClick={()=>this.selectAll()}>{'全选'}</li>
                        <li onClick={()=>this.removeSelectAll()}>{'删除选中'}</li>
                    </ul>
                </div>
            );
        } else {
            return ''
        }
    }

    renderItem() {
        let content = this.state.data.map((item, i) => {
            return(
                    <li  className={'cellWrapper'}>
                        <div onClick={()=>this.addDeleteID(item)} className={this.state.edit?'editBox':'hide'}>
                            <div className={this.isSelectDelete(item)?'editBox-circle-select':'editBox-circle'}></div>
                        </div>
                        <div>
                            <Link to={'/detail/' + item.id} className={'cell'}>
                                <img src={item.thumb} alt={''}/>
                                <div className={'contentWrapper'}>
                                    <div className={'title'}>{item.title}</div>
                                </div>
                            </Link>
                            <div className={'cellWrapper-bottom'}>
                                <div className={'dateWrapper'}>
                                    <div className={'date'}>{item.date}</div>
                                </div>
                                <div onClick={()=>this.chooseStar(item)} className={'starBox'}>
                                    <div className={'starBox-text'}>{this.isSameItem(item) ? '取消收藏':'收藏'}</div>
                                    <div className={this.isSameItem(item) ? 'starBox-iconBox purple':'starBox-iconBox'}>
                                        <Svg path={love}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
            );
        });
        return content;
    }

    render(){

        if (this.state.data.length === 0) {
            return(
                <div className={'news'}>
                    <Header {...this.props} title={'浏览记录'}/>
                    <div className={'emptyBox'}>
                        <Svg path={empty}/>
                        <div>暂无数据</div>
                    </div>
                </div>
            );
        }

        //样式文件是live/index.scss
        return(
            <div className={'news ul-content-paddingTop-offset'}>
                <Header {...this.props} title={'我的收藏'} customRight={{title:this.state.edit?'完成':'编辑',callback:()=>this.setState({edit:!this.state.edit})}}/>
                <ul className={this.state.edit?'ul-content-offset':''}>
                    {this.renderItem()}
                </ul>
                {this.renderEditableButton()}
            </div>
        );
    }

    isSelectDelete(item){
        let result = this.state.deleteIDs.find(element=>{
            return element === item.id;
        });
        return result;
    }

    addDeleteID(item){
        let result = this.state.deleteIDs.find(element=>{
            return element === item.id;
        });

        let temp = [].concat(this.state.deleteIDs);
        if (result !== undefined) {
            temp.splice(item,1);
        }else{
            temp.push(item.id);
        }

        this.setState({
            deleteIDs:temp
        });
    }

    selectAll(){
        let result = []
        for (let i = 0; i < this.state.data.length; i++) {
            result.push(this.state.data[i].id);
        }

        this.setState({
            deleteIDs:result
        });
    }

    removeSelectAll(){
        // this.setState({
        //     deleteIDs:[]
        // });
        this.removeFavorite()
    }

    removeFavorite(){

        this.setState({edit:!this.state.edit})

        if (this.state.edit === false) {
            return;
        }

        let result = [];
        for (let i = 0; i < this.state.data.length; i++) {
            const element = this.state.data[i];
            //检测是不是位于删除列表
            let tempResult = this.isSelectDelete(element);
            //如果不在删除列表中的元素则添加进新数组
            if (!tempResult) {
                result.push(element)
            }
        }
        this.setState({
            data:result,
        });
        let jsonString = JSON.stringify(result);
        localStorage.setItem('record',jsonString);
    }

    isSameItem(item){
        let ary = JSON.parse(localStorage.getItem('news')) || [];
        let result = ary.find(((element)=>{
            return element.id === item.id;
        }))
        return result;
    }

    //todo 选择自己喜欢
    chooseStar(item) {
        let ary = JSON.parse(localStorage.getItem('news')) || [];
        //todo 先检查是否存在，如果存在就移除， 如果不存在就添加
        let hehe = ary.find(((element)=>{
            return element.id === item.id;
        }))

         if (hehe) {
            ary.splice(item, 1);
            AlertFunction({title: '提示', msg: '取消成功'})
         }else{
            ary.push(item)
            AlertFunction({title: '提示', msg: '添加成功'})
         }

        // this.setState({active: !this.state.active});
        let Ary = JSON.stringify(ary);
        localStorage.setItem('news', Ary);
        this.setState({});
    }
}