import React, {Component} from 'react';
import {Link,Redirect} from 'react-router-dom';
import {Svg, Footer} from "../common";
import Header from '../common/header';
import {Cache} from "../../module";
import {Schedule, Req} from "../../lib";
import AlertFunction from "../../lib/AlertFunction";
//todo 引入图片

import news from '../../images/Nf-svg/news.svg';
import server from '../../images/Nf-svg/server.svg';
import share from '../../images/Nf-svg/share.svg';
import password from '../../images/Nf-svg/password.svg';
import logo from '../../images/mdf-svg/userLogo.svg';
import QQ from '../../images/Nf-img/qq.png';
import wechat from '../../images/Nf-img/wechat.png';
import social from '../../images/Nf-img/social.png';
import rightArrow from './../../images/xlt/right-arrow.svg';

import quota from './../../images/xlt/my-notice.svg'
import invite from './../../images/xlt/my-record.svg'
import live from './../../images/xlt/my-star.svg'
import contact from './../../images/xlt/contact.svg'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            name: '',
            bankCardCount: '',
            aLiPay: '',
            phone: '',
            withdrawPass: '',
            userLevel: Cache.userLevel,
            userName: Cache.username,
            userId: Cache.userId,
            balance: Cache.gameBalance,
            shareShow:false,

            mobile:'',
            email:'',
            comment:''
        };
    }

    render() {
        return (
            <div className={'accountSet'}>
                <Header title={'个人资料'} back={'/'}/>
                <div className={this.state.isLogin ? 'top afterLogin' :'top beforeLogin'}>
                    {/* <div>
                        <Svg path={logo}/>
                    </div> */}
                    {
                        this.state.isLogin ? (
                            <div className={'settingBox'}>
                                {/* <div className={'userNameBox'}>
                                    <a>{this.state.userName || '-'}</a>
                                    <a>暂无个人简介</a>
                                </div>
                                <div className={'settingBox-changePassword'}>
                                    <Link to={'/changePassword'}>修改密码</Link>
                                    <dic className={'settingBox-changePassword-iconBox'}>
                                        <Svg path={rightArrow}/>
                                    </dic>
                                </div> */}
                                <ul className={'dataList'}>
                                    <li>
                                        <div>用户名</div>
                                        <div>{this.state.userName || '-'}</div>
                                    </li>
                                    <li>
                                        <div>模拟币</div>
                                        <div className={'balanceBox'}>
                                            <div>{this.state.balance || '-'}</div>
                                            <div onClick={()=>this.addSimBalance()}>一键加币</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        ) : (
                            <Redirect to={'/login'}/>
                        )
                    }

                </div>

                {/* <div className={'topButtonBoxs'}>
                    <Link to={'/cs'} className={'home-quota'}>
                        <div className={'image imageSize'}>
                            <Svg path={quota}/>
                        </div>
                        <div className={'text'}>我的消息</div>
                    </Link>
                    <Link to={'/Record'} className={'home-invite'}>
                        <div className={'image imageSize'}>
                            <Svg path={invite}/>
                        </div>
                        <div className={'text'}>浏览记录</div>
                    </Link>
                    <Link to={'/Favorite'} className={'home-live'}>
                        <div className={'image imageSize'}>
                            <Svg path={live}/>
                        </div>
                        <div className={'text'}>我的收藏</div>
                    </Link>
                    <Link to={'/contact'} className={'home-live'}>
                        <div className={'image imageSize'}>
                            <Svg path={contact}/>
                        </div>
                        <div className={'text'}>留言联系</div>
                    </Link>
                </div> */}

                 {/* {
                    this.state.isLogin ? (<div className={'button'} onClick={() => this.logout()}>
                        <div>退出登录</div>
                    </div>) : ('')
                } */}
           
                {/* <div className={'commentBox'}>
                    <textarea value={this.state.comment} onChange={e=>this.setState({comment:e.currentTarget.value})} placeholder={'留言内容'} readOnly={false} cols={'30'}/>
                    <div className={'tips'}>*请填写您的联系方式</div>
                    <div className={'inputData'}>
                        <div className={'inputBox'}>
                            <span>
                                手机&nbsp;(必填)
                            </span>
                            <input value={this.state.mobile} onChange={e=>this.setState({mobile:e.currentTarget.value})} placeholder={'请填写你的手机号码'} type={'tel'}/>
                        </div>
                        <div className={'inputBox'}>
                            <span>
                                电邮 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span>
                            <input  value={this.state.email} onChange={e=>this.setState({email:e.currentTarget.value})} placeholder={'请填写你的手机号码'} type={'tel'}/>
                        </div>
                    </div>
                    <div onClick={()=>this.submit()} className={'submit'}>提交</div>
                </div> */}
                
                {/* <div className={'list'}>
                    <Link to={{pathname:'/position/position',state:{page:'position'}}}>
                        <div className={'listLeft'}>
                            <Svg path={editPassword} className={'arrow'} status={'rotate'}/>
                            <div>我的持仓</div>
                        </div>
                    </Link>
                    <Link to={{pathname:'/position/position',state:{page:'end'}}}>
                        <div className={'listLeft'}>
                            <Svg path={editPassword} className={'arrow'} status={'rotate'}/>
                            <div>结算列表</div>
                        </div>
                    </Link>
                    <Link to={'/changePassword'}>
                        <div className={'listLeft'}>
                            <Svg path={editPassword} className={'arrow'} status={'rotate'}/>
                            <div>修改密码</div>
                        </div>
                    </Link>
                </div> */}
                {/* <Footer/> */}
            </div>
        )
    }

    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback();
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this);
        }
        Schedule.addEventListener('getUserInfo', this.getInfoCallback, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this)
    }

    //todo 退出
    logout() {
        AlertFunction({title: '警告', msg: '是否退出账号？', hasCancel: true, confirm: () => Cache.logout()})
    }

    loginCallback() {
        this.setState({
            isLogin: Cache.isLogin()
        });
        this.getInfoCallback();
    }

    //todo 一键加币功能
    async addSimBalance() {
        try {
            const result = await Req({
                url: '/api/trade/addScore.htm',
                animate: true
            });
            Cache.getUserInfo();
            AlertFunction({title: '提示', msg: result.resultMsg});
        } catch (err) {
            AlertFunction({title: '错误', msg: err.resultMsg});
        }
    }

    getInfoCallback() {
        this.setState({
            name: Cache.realName,
            bankCardCount: Cache.bankCardCount,
            phone: Cache.phone,
            withdrawPass: Cache.withdrawPass,
            userLevel: Cache.userLevel,
            userName: Cache.username,
            userId: Cache.idNumber,
            balance: Cache.gameBalance
        })
    }

    submit(){
        if (this.state.comment.length === 0) {
            return AlertFunction({title: '提示', msg: '请输入留言'});
        }

        if (this.state.mobile.length === 0) {
            return AlertFunction({title: '提示', msg: '请输入手机号'});
        }

        if (this.state.email.length === 0) {
            return AlertFunction({title: '提示', msg: '请输入电邮'});
        }
        AlertFunction({title: '提示', msg: '提交成功'});
        this.setState({
            comment:'',
            mobile:'',
            email:''
        });
    }

}