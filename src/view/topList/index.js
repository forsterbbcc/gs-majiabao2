import React, {Component} from 'react';
import {Header} from "../common";

//todo 图片
import topBg from '../../images/Nf-img/top.png';
import top from '../../images/Nf-img/topOne.png';
import top1 from '../../images/Nf-img/top1.png';
import top2 from '../../images/Nf-img/top2.png';
import top3 from '../../images/Nf-img/top3.png';
import user from '../../images/Nf-img/user.png';


export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <div className={'topList'}>
                <Header title={'榜单排名'} {...this.props}/>
                <div className={'main'}>
                    <img src={topBg} alt=""/>
                    <div className={'top'}>
                        <img src={top1} alt=""/>
                        <img src={top} alt=""/>
                        <div>
                            <div className={'name'}>牛牛牛A</div>
                            <div className={'percent'}>62.8%</div>
                            <div className={'last'}>
                                <div>月收益</div>
                            </div>
                        </div>
                    </div>
                    <ul>
                        <li>
                            <div className={'left'}>
                                <img src={top2} alt=""/>
                                <img src={user} alt=""/>
                                <div>1978</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>61.4%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                        <li>
                            <div className={'left'}>
                                <img src={top3} alt=""/>
                                <img src={user} alt=""/>
                                <div>a274885</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>60.2%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                        <li>
                            <div className={'left'}>
                                <div className={'num'}>04</div>
                                <img src={user} alt=""/>
                                <div>梦回唐朝</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>60.1%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                        <li>
                            <div className={'left'}>
                                <div className={'num'}>05</div>
                                <img src={user} alt=""/>
                                <div>超级火神</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>59.9%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                        <li>
                            <div className={'left'}>
                                <div className={'num'}>06</div>
                                <img src={user} alt=""/>
                                <div>陈嫦</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>59.2%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                        <li>
                            <div className={'left'}>
                                <div className={'num'}>07</div>
                                <img src={user} alt=""/>
                                <div>溜溜吧</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>58.8%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                        <li>
                            <div className={'left'}>
                               <div className={'num'}>08</div>
                                <img src={user} alt=""/>
                                <div>金陵小哥</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>58.3%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                        <li>
                            <div className={'left'}>
                               <div className={'num'}>09</div>
                                <img src={user} alt=""/>
                                <div>长翅膀的大灰猫</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>58.1%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                        <li>
                            <div className={'left'}>
                                <div className={'num'}>10</div>
                                <img src={user} alt=""/>
                                <div>大山叔叔爱萝莉</div>
                            </div>
                            <div className={'right'}>
                                <div className={'percent'}>57.7%</div>
                                <div>月收益</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }

    componentDidMount() {

    }
}