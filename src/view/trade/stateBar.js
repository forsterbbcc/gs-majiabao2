import React, {Component} from 'react';
import {Link , Redirect} from 'react-router-dom';
import loading from '../../images/gif/loading.gif';

import {Schedule, Req} from "../../lib";
import {Contracts, Cache, Rest} from "../../module";
import {getIdentity, getPlatform} from "../../lib/tool";
import AlertFunction from "../../lib/AlertFunction";
import plus from './../../images/Nf-svg/plus3.svg'
import news from './../../images/xlt/trade-news.svg'
import share from './../../images/Nf-svg/rule.svg'
import { Svg } from '../common';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasFastTrade: Cache.tradeQuick,
            fastTrade: false,
            rest: false,
            isLogin: null,
            balance: this.props.location.state.simulate ? Cache.gameBalance : Cache.realBalance,
            simulate: this.props.location.state.simulate,
            moneyType: 0,
            hasMoneyType: 1,
            buyVolume: 0,
            buyPrice: 0,
            buyWidth: 1,
            sellVolume: 0,
            sellPrice: 0,
            sellWidth: 1,
            stopLossIndex: 0,
            stopLoss: [0],
            stopProfit: [0],
            volumeIndex: 0,
            volume: [0],

            showStopProfit:false,
            showVolume:false,
            redirectLogin:false
        };
        if (Cache.initial) {
            this.state.isLogin = Cache.isLogin()
        }
        if (!!Cache.tradeList) {
            this.updateSchemeInfo(true);
        }
        let c = Contracts.getContract(this);
        if (!!c) {
            this.state.hasMoneyType = Contracts.total[c].moneyType;
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }

    renderStopProfit(show,dataSource,title,callback){
        if (show) {
            let isEven = dataSource.length % 2 === 0
            return(
                <div className={'stopProfitTable'}>
                    {
                        dataSource.map((item,i)=>{
                            if(isEven){
                                let isLight = i%2 === 0 ?  ' titleWrapperSecondBackground' : ' titleWrapperFirstBackground'
                                return(
                                    <div onClick={()=>callback(i)} className={'titleWrapper'+isLight}>
                                        <div>{title}</div>
                                        <div>{item}</div>
                                    </div>
                                );
                            }else{
                                let isLight = i%2 === 0 ?  ' titleWrapperFirstBackground' : ' titleWrapperSecondBackground'
                                return(
                                    <div onClick={()=>callback(i)}  className={'titleWrapper'+isLight}>
                                        <div>{title}</div>
                                        <div>{item}</div>
                                    </div>
                                )
                            }
                        })
                    }
                </div>
            );
        }else{
            return '';
        }
    }

    renderStopLoss(){
        return this.state.stopLoss.map((item,i)=>{
            return(
                <div onClick={()=>this.setState({stopLossIndex:i})} className={i === this.state.stopLossIndex ? 'stopLossActive stopLossButton':'stopLossButton'}>{item.mul(this.state.volume[this.state.volumeIndex]).mul(this.state.moneyType === 0 ? 1 : 0.1)}</div>
            );
        });
    }

    renderTradeVolume(){
        return(
            <div className={'volumeBox'}>
                <div className={'buttonBox'}>
                    <div onClick={()=>this.switchVolume(false)} className={'button'}>-</div>
                </div>
                <div className={'value'}>{this.state.volume[this.state.volumeIndex]}</div>
                <div className={'buttonBox'}>
                    <div onClick={()=>this.switchVolume(true)} className={'button'}>+</div>
                </div>
            </div>
        );
    }

    renderInfoVersion(){
        return(
            <ul className={'infoButton'}>
                <li   className={this.getAddButtonText(this.props.location.state.contract) === '添加' ? '':'favoriteAlready'}>
                    <div onClick={()=>this.chooseStar(this.props.location.state.contract)} className={'addButton button'}>
                        <div className={'buttonImage'}>
                            <Svg path={plus}/>
                        </div>
                        <div>{this.getAddButtonText(this.props.location.state.contract)}</div>
                    </div>
                </li>
                <li>
                    <Link to={'/live/news'} className={'newsButton button'}>
                        <div className={'buttonImage'}>
                            <Svg path={news}/>
                        </div>
                        <div>新闻</div>
                    </Link>
                </li>
                <li>
                    <div onClick={()=>this.props.active()} className={'shareButton button'}>
                        <div className={'buttonImage'}>
                            <Svg path={share}/>
                        </div>
                        <div>规则</div>
                    </div>
                </li>
            </ul>
        );
    }

    render() {

        if (this.state.redirectLogin) {
            return(
                <Redirect to={'/login'}/>
            );
        }

        if (this.state.isLogin === false) {
            return(
                <div className={'loginBar'}>
                    <div className={'loginBar-top'}>
                        <Link to={'/login'}>登&nbsp;&nbsp;录</Link>
                        <Link to={'/register'}>注&nbsp;&nbsp;册</Link>
                    </div>

                    <div className={'stateBar-bottom'}>
                        <Link to={'/login'}>
                            <div>买涨</div>
                        </Link>
                        <Link to={'/login'}>
                            <div>买跌</div>
                        </Link>
                        <Link className={'stateBar-bottom-position'} to={'/login'}>
                            <div>持仓</div>
                        </Link>
                    </div>
                </div>
            );
        }
        return (
            <div className={'stateBar'}>
                <div className={'stateBar-top'}>
                    <ul>
                        <li className={'cell'}>
                            <div className={'cell-left'}>交易手数</div>
                            <div className={'cell-right'}>
                                {this.renderTradeVolume()}
                            </div>
                        </li>
                        <li className={'cell'}>
                            <div className={'cell-left'}>触发止盈</div>
                            <div className={'cell-right'}>
                                {this.state.stopProfit[this.state.stopLossIndex].mul(this.state.volume[this.state.volumeIndex]).mul(this.state.moneyType === 0 ? 1 : 0.1)}
                            </div>
                        </li>
                        <li className={'cell'}>
                            <div className={'cell-left'}>触发止损</div>
                            <div className={'cell-right'}>
                                {this.renderStopLoss()}
                            </div>
                        </li>
                    </ul>
                </div>
                <div className={'stateBar-bottom'}>
                    <div>
                        <div onClick={()=>this.submit(true)}>买涨</div>
                    </div>
                    <div>
                        <div onClick={()=>this.submit(false)}>买跌</div>
                    </div>
                    <Link className={'stateBar-bottom-position'} to={{pathname:'/position',state:{page:'position'}}}>
                        <div>持仓</div>
                    </Link>
                </div>
            </div>
        )
    }

    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback()
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this);
        }
        if (this.state.stopLoss.length === 1 && !!Cache.tradeList) {
            this.updateSchemeInfo();
        } else {
            Schedule.addEventListener('getSchemeInfo', this.updateSchemeInfo, this);
        }
        Schedule.addEventListener('loginCallback', this.loginCallback, this);
        Schedule.addEventListener('quoteUpdate', this.updateQuote, this);
        Schedule.addEventListener('getUserInfo', this.updateUserInfo, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location.state.simulate !== nextProps.location.state.simulate) {
            this.setState({
                simulate: nextProps.location.state.simulate,
                balance: nextProps.location.state.simulate ? Cache.gameBalance : Cache.realBalance
            });
        }
        if (this.props.location.state.contract !== nextProps.location.state.contract) {
            const o = Contracts.total[nextProps.location.state.contract];
            this.setState({
                hasMoneyType: o.moneyType,
                moneyType: 0
            });
            if (Cache.isLogin()) {
                let o = nextProps.location.state.contract;
                if (!o) {
                    [o] = Contracts.foreignArray;
                    o = o.contract;
                }
                let scheme = Cache.tradeList[o];
                this.setState({
                    hasFastTrade: Cache.tradeQuick,
                    volume: scheme.volumeList,
                    stopLoss: scheme.stopLossList,
                    stopProfit: scheme.stopProfitList
                })
            }
        }
    }
    switchVolume(isAdd){
        if (isAdd) {
            if (!(this.state.volumeIndex === this.state.volume.length-1)) {
                this.setState({
                    volumeIndex:this.state.volumeIndex+1
                });
            }
        } else {
            if (!(this.state.volumeIndex === 0)) {
                this.setState({
                    volumeIndex:this.state.volumeIndex-1
                });
            }
        }
    }

    switchFastTrade() {
        this.setState({fastTrade: !this.state.fastTrade})
    }

    switchMoneyType(v) {
        this.setState({moneyType: v})
    }

    showStopLossSelection(){
        this.setState({
            showStopProfit:!this.state.showStopProfit
        });
    }

    showVolumeSelection(){
        this.setState({
            showVolume:!this.state.showVolume
        });
    }

    chooseStopLossIndex(value){
        this.setState({
            stopLossIndex:value,
            showStopProfit:!this.state.showStopProfit
        });
    }
    
    chooseVolume(value){
        this.setState({
            volumeIndex:value,
            showVolume:!this.state.showVolume
        });
    }

    //todo 选择自己喜欢
    chooseStar(contract) {
        if (Contracts.initial) {
            console.log(JSON.stringify(Contracts.total))
            let code = Contracts.total[contract].code;
            let ary = JSON.parse(localStorage.getItem('self')) || [];
            //todo 先检查是否存在，如果存在就移除， 如果不存在就添加

            if (ary.includes(code)) {
                ary.remove(code)
                AlertFunction({title: '提示', msg: '取消成功'})
            }else{
                ary.push(code)
                AlertFunction({title: '提示', msg: '添加成功'})
            }
            this.setState({active: !this.state.active});
            let Ary = JSON.stringify(ary);
            localStorage.setItem('self', Ary);
            Contracts.updateSelf()
        }
    }

    getAddButtonText(contract){
        if (Contracts.initial) {
            let code = Contracts.total[contract].code;
            let ary = JSON.parse(localStorage.getItem('self')) || [];
            //todo 先检查是否存在，如果存在就移除， 如果不存在就添加

            if (ary.includes(code)) {
                return '取消收藏'
            }else{
                return '添加'
            }
        }else{
            return '添加'
        }
    }

    async submit(isBuy){

        if (!this.state.isLogin) {
            return this.setState({redirectLogin:true});
        }

        try {
            const o = this.props.location.state.contract;
            const obj = Contracts.total[o];
            const scheme = Cache.tradeList[o];
            const handle = this.state.volume[this.state.volumeIndex];
            const result = await Req({
                url: '/api/trade/open.htm',
                type: 'POST',
                data: {
                    identity: getIdentity(16),
                    tradeType: 2,//this.props.location.state.simulate ? 2 : 1,//模拟交易2 实盘交易1
                    source: '下单',  // 买入来源（下单、反向、快捷)
                    commodity: obj.code,
                    contract: o,
                    isBuy: isBuy,
                    price: 0,
                    stopProfit: this.state.stopProfit[this.state.stopLossIndex].mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    stopLoss: this.state.stopLoss[this.state.stopLossIndex].mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    serviceCharge: scheme.chargeUnit.mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    eagleDeduction: 0,
                    volume: handle,
                    moneyType: this.state.moneyType,
                    platform: getPlatform()
                },
                animate: true
            });
            AlertFunction({title: '提示', msg: result.errorMsg});
            Cache.getUserInfo();

        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg});
        }
    }

    /**
     * 更新成交量及价格
     * @param e
     */
    updateQuote(e) {
        const {priceDigit} = Contracts.total[e.code];
        const total = e.wt_buy_volume.add(e.wt_sell_volume);
        this.setState({
            rest: !Rest.isOpening(Contracts.total[e.code]),
            buyPrice: e.wt_sell_price.toFixed(priceDigit),
            buyVolume: e.wt_buy_volume,
            buyWidth: e.wt_buy_volume.div(total).mul(80),
            sellPrice: e.wt_buy_price.toFixed(priceDigit),
            sellVolume: e.wt_sell_volume,
            sellWidth: e.wt_sell_volume.div(total).mul(80),
            item:e.item
        })
    }

    /**
     * 更新合约支持交易货币类型  元/角
     */
    updateContracts() {
        let c = Contracts.getContract(this);
        if (!!c) {
            this.setState({
                hasMoneyType: Contracts.total[c].moneyType
            })
        }
    }

    /**
     * 更新购买信息以及是否有快速交易
     */
    updateSchemeInfo(init) {
        let c = Contracts.getContract(this);
        let scheme = Cache.tradeList[c];
        if (init) {
            this.state.hasFastTrade = Cache.tradeQuick;
            this.state.volume = scheme.volumeList;
            this.state.stopLoss = scheme.stopLossList;
            this.state.stopProfit = scheme.stopProfitList;
        } else {
            this.setState({
                hasFastTrade: Cache.tradeQuick,
                volume: scheme.volumeList,
                stopLoss: scheme.stopLossList,
                stopProfit: scheme.stopProfitList
            })
        }
    }

    /**
     * 更新用户信息
     */
    updateUserInfo() {
        this.setState({
            balance: this.state.simulate ? Cache.gameBalance : Cache.realBalance
        })
    }

    /**
     * 登录回调
     */
    loginCallback() {
        this.setState({
            isLogin: Cache.isLogin(),
            hasFastTrade: Cache.tradeQuick
        })
    }

    async addSimBalance() {
        try {
            const result = await Req({
                url: '/api/trade/addScore.htm',
                animate: true
            });
            Cache.getUserInfo();
            AlertFunction({title: '提示', msg: result.resultMsg});
        } catch (err) {
            AlertFunction({title: '错误', msg: err.resultMsg});
        }
    }

    /**
     * 快速交易
     */
    async fastTrade(isBuy) {
        try {
            const o = this.props.location.state.contract;
            const obj = Contracts.total[o];
            const scheme = Cache.tradeList[o];
            const handle = this.state.volume[this.state.volumeIndex];
            const result = await Req({
                url: '/api/trade/open.htm',
                type: 'POST',
                data: {
                    identity: getIdentity(16),
                    tradeType: this.props.location.state.simulate ? 2 : 1,//模拟交易2 实盘交易1
                    source: '下单',  // 买入来源（下单、反向、快捷)
                    commodity: obj.code,
                    contract: o,
                    isBuy: isBuy,
                    price: 0,
                    stopProfit: this.state.stopProfit[this.state.stopLossIndex].mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    stopLoss: this.state.stopLoss[this.state.stopLossIndex].mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    serviceCharge: scheme.chargeUnit.mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    eagleDeduction: 0,
                    volume: handle,
                    moneyType: this.state.moneyType,
                    platform: getPlatform()
                },
                animate: true
            });
            AlertFunction({title: '提示', msg: result.errorMsg});
            Cache.getUserInfo();
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg});
        }
    }

}