import React, {Component} from 'react';
import {Chart, Contracts, Quotes} from "../../module";
import {Schedule, Req} from "../../lib";

import {Link} from 'react-router-dom';
import Header from './header';
import QuoteBar from './quoteBar';
import ChartBar from './chartBar';
import StateBar from './stateBar';
import Dynamic from './dynamic';
import Rule from '../rule';
import Order from '../order';
import { Svg } from '../common';
import search from '../../images/Nf-svg/search.svg';
export default class App extends Component {
    _ref = null;
    constructor(props) {
        super(props);
        this._init = false;
        this.state = {
            select: false,
            dynamic: 'hide',
            foreignArray: [],
            domesticArray: [],
            stockArray: [],
            rule:false,
            order:false,
            fuck:'',
            searchShow:false,
            searchInfo: ''
        };
        let c = Contracts.getContract(this);
        if(!!c){
            this.state.foreignArray = Contracts.foreignArray;
            this.state.domesticArray = Contracts.domesticArray;
            this.state.stockArray = Contracts.stockArray;
            this._init = true;
        }
        this.order = this.order.bind(this);
    }

    render() {
        return (
            <div className={'trade'}>
                {
                    // 切换商品界面
                    this.state.select ? (
                        <div className={'mask'}>
                            <div className={'space'} onClick={this.closeSelect.bind(this)}/>
                            <div className={'list'}>
                                <ul>
                                    <li>
                                        <div className={'title'}>国际期货</div>
                                        <div className={'group'}>
                                            {
                                                this.state.foreignArray.map((c) => {
                                                    return (
                                                        <div onClick={() => this.closeSelect()}>
                                                            <Link to={{
                                                                pathname: '/trade',
                                                                state: {
                                                                    contract: c.contract,
                                                                    name:c.name,
                                                                    simulate: true
                                                                }
                                                            }}>
                                                                {c.name}
                                                            </Link>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </li>
                                    <li>
                                        <div className={'title'}>股指期货</div>
                                        <div className={'group'}>
                                            {
                                                this.state.stockArray.map((c) => {
                                                    return (
                                                        <div onClick={() => this.closeSelect()}>
                                                            <Link to={{
                                                                pathname: '/trade',
                                                                state: {
                                                                    contract: c.contract,
                                                                    name:c.name,
                                                                    simulate: true
                                                                }
                                                            }}>
                                                                {c.name}
                                                            </Link>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </li>
                                    <li>
                                        <div className={'title'}>国内期货</div>
                                        <div className={'group'}>
                                            {
                                                this.state.domesticArray.map((c) => {
                                                    return (
                                                        <div onClick={() => this.closeSelect()}>
                                                            <Link to={{
                                                                pathname: '/trade',
                                                                state: {
                                                                    contract: c.contract,
                                                                    name:c.name,
                                                                    simulate: true
                                                                }
                                                            }}>
                                                                {c.name}
                                                            </Link>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    ) : ''
                }
                <Header  active={()=>Schedule.dispatchEvent({event:'openSearch'})} {...this.props} openSelect={() => this.openSelect()}/>
                <ChartBar {...this.props} dynamic={(status) => this.dynamic(status)}/>
                <div className={'chart'}>
                    <Dynamic className={this.state.dynamic}/>
                    <div id={'tradingView'} ref={(e)=>this._ref = e}/>
                </div>
                <StateBar active={()=>this.setState({rule:true})} {...this.props} order={this.order}/>
                <Rule show={this.state.rule} close={()=>this.setState({rule:false})} {...this.props}/>
                {/* <Order show={this.state.order} close={()=>this.setState({order:false})} {...this.props}/> */}
            </div>
        )
    }

    componentDidMount() {
        if(this._init || this.props.location.state.code){
            
            let defaultCode = this.state.foreignArray.find(element=>{
                return element.code;
            });
            
            Quotes.start('quoteUpdate', this.props.location.state.contract || defaultCode);
            this.startTradingView();

        } else {
            let c = Contracts.getContract(this);

            if(!!c){
                Quotes.start('quoteUpdate', c);
                this.setState({
                    foreignArray: Contracts.foreignArray,
                    domesticArray: Contracts.domesticArray,
                    stockArray: Contracts.stockArray
                });
                this.startTradingView();
                
            }else {
                Schedule.addEventListener('contractsInitial', this.updateContracts, this);
                Schedule.addEventListener('contractsInitial', this.startTradingView, this);
            }

        }
    }

    componentWillUnmount() {
        Chart.exit();
        Quotes.end();
        Schedule.removeEventListeners(this);
    }

    dynamic(status) {
        this.setState({dynamic: status})
    }

    openSelect() {
        this.setState({select: true})
    }

    closeSelect() {
        this.setState({select: false})
    }

    updateContracts() {
        let c = Contracts.getContract(this);
        if(!!c){
            Quotes.start('quoteUpdate', c);
            this.setState({
                foreignArray: Contracts.foreignArray,
                domesticArray: Contracts.domesticArray,
                stockArray: Contracts.stockArray
            });
        }
    }

    startTradingView() {
        const code = Contracts.total[this.props.location.state.contract].code;
        console.log(code);
        Chart.init({
            dom: 'tradingView',
            code: code,
            height:this._ref.scrollHeight,
            width:this._ref.scrollWidth
        });
    }

    order(isBuy){
        this.props.location.state.isBuy = isBuy;
        this.setState({
            order:true
        })
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location.state.contract !== nextProps.location.state.contract) {
            Quotes.switch(nextProps.location.state.contract);
            const code = Contracts.total[nextProps.location.state.contract].code;
            Chart.swap({code: code})
        }
    }
}