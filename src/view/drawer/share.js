import React,{Component} from 'react';

import QQ from '../../images/Nf-img/qq.png';
import wechat from '../../images/Nf-img/wechat.png';
import social from '../../images/Nf-img/social.png';
import { Schedule } from '../../lib';

export default class App extends Component{
    constructor(props){
        super(props);

        this.state = {
            shareShow:false
        }
    }

    componentDidMount(){
        Schedule.addEventListener('openShare',this.show,this);
    }

    componentWillUnmount(){
        Schedule.removeEventListener(this);
    }

    show(){
        this.setState({
            shareShow:!this.state.shareShow
        });
    }

    render(){
        if (this.state.shareShow === false) {
            return '';
        }

        return(
            <div className={'shareBox'} onClick={()=>this.setState({shareShow:false})}>
                <div className={'shareMain'}>
                    <div onClick={()=>this.QQ('分享')}>
                        <img src={QQ} alt=""/>
                        <div>QQ</div>
                    </div>
                    <div onClick={()=>this.weChat()}>
                        <img src={wechat} alt=""/>
                        <div>微信</div>
                    </div>
                    <div  onClick={()=>this.sinaWebo('分享')}>
                        <img src={social} alt=""/>
                        <div>朋友圈</div>
                    </div>
                </div>
            </div>
        );
    }

    weChat() {
        let target_url = "http://qr.liantu.com/api.php?text=http://test.qicheyitiao.com";
        window.open(target_url, 'weChat', 'height=300, width=300');
    }

    QQ(title, url, pic) {
        let p = {
            url: 'http://test.qicheyitiao.com', /*获取URL，可加上来自分享到QQ标识，方便统计*/
            desc: '来自汽车氪的分享', /*分享理由(风格应模拟用户对话),支持多分享语随机展现（使用|分隔）*/
            title: title, /*分享标题(可选)*/
            summary: title, /*分享描述(可选)*/
            pics: pic, /*分享图片(可选)*/
            flash: '', /*视频地址(可选)*/
            //commonClient : true, /*客户端嵌入标志*/
            site: '新势力'/*分享来源 (可选) ，如：QQ分享*/
        };


        let s = [];
        for (let i in p) {
            s.push(i + '=' + encodeURIComponent(p[i] || ''));
        }
        let target_url = "http://connect.qq.com/widget/shareqq/iframe_index.html?" + s.join('&');
        window.open(target_url, 'QQ', 'height=320, width=320');
    }

    sinaWebo(title,url,picurl){
        var sharesinastring='http://v.t.sina.com.cn/share/share.php?title='+title+'&url='+url+'&content=utf-8&sourceUrl='+url+'&pic='+picurl;
        window.open(sharesinastring,'newwindow','height=300,width=300,top=100,left=100');
    }
}