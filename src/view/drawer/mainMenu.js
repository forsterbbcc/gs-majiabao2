import React,{Component} from 'react';
import './../../../node_modules/rc-drawer/assets/index.css';
import Drawer from 'rc-drawer';
import { Schedule } from '../../lib';
import { Svg } from '../common';
import {Link} from 'react-router-dom';

import home from './../../images/qh/menu-home.svg';
import mine from './../../images/qh/menu-mine.svg';
import edit from './../../images/qh/menu-editPassword.svg';
import about from './../../images/qh/menu-about.svg';
import notice from './../../images/qh/home-notice.svg';
import share from './../../images/qh/home-share.svg';
import login from './../../images/qh/login.svg'
import logout from './../../images/qh/home-logout.svg';
import { Cache } from '../../module';
import AlertFunction from '../../lib/AlertFunction';
export default class App extends Component{
    constructor(props){
        super(props);

        this.state = {
            open:false,
            isLogin:Cache.isLogin()
        }
    }

    componentDidMount(){
        Schedule.addEventListener('openDrawer',this.onSwitch,this);
        if (this.state.isLogin) {
            this.loginCallback()
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this);
        }
    }

    componentWillUnmount(){
        Schedule.removeEventListener(this);
    }

    onTouchEnd = () => {
        this.setState({
          open: false,
        });
      }
      onSwitch = () => {
        this.setState({
          open: !this.state.open,
        });
      }

    render(){
        return(
            <Drawer ref={view=>this.drawer=view} width={'70%'} open={this.state.open} handler={false} onMaskClick={this.onTouchEnd}>
                <div className={'mainMenu'}> 
                    <ul>
                        <li onClick={this.onTouchEnd}>
                            <Link to={'/'}>
                                <div className={'imageBox'}>
                                    <Svg path={home}/>
                                </div>
                                <div className={'title'}>主页</div>
                            </Link>
                        </li>
                        <li onClick={this.onTouchEnd}>
                            <Link to={'/accountSet'}>
                                <div className={'imageBox'}>
                                    <Svg path={mine}/>
                                </div>
                                <div className={'title'}>个人资料</div>
                            </Link>
                        </li>
                        <li onClick={this.onTouchEnd}>
                            <Link to={'/changePassword'}>
                                <div className={'imageBox'}>
                                    <Svg path={edit}/>
                                </div>
                                <div className={'title'}>修改密码</div>
                            </Link>
                        </li>
                        <li onClick={this.onTouchEnd}>
                            <Link to={'/live/lives'}>
                                <div className={'imageBox'}>
                                    <Svg path={about}/>
                                </div>
                                <div className={'title'}>资讯</div>
                            </Link>
                        </li>
                        <li onClick={this.onTouchEnd}>
                            <Link to={'/notice'}>
                                <div className={'imageBox'}>
                                    <Svg path={notice}/>
                                </div>
                                <div className={'title'}>公告</div>
                            </Link>
                        </li>
                        {/* <li onClick={()=>{
                            Schedule.dispatchEvent({event:'openShare'});
                            this.onTouchEnd();
                        }}>
                            <a>
                                <div className={'imageBox'}>
                                    <Svg path={share}/>
                                </div>
                                <div className={'title'}>分享</div>
                            </a>
                        </li> */}
                        <li onClick={()=>{
                            if (this.state.isLogin) {
                                AlertFunction({title: '警告', msg: '是否退出账号？', hasCancel: true, confirm: () => Cache.logout()});
                            }
                            this.onTouchEnd();
                        }}>
                            <Link to={this.state.isLogin?'/':'/login'}>
                                <div className={'imageBox'}>
                                    <Svg path={this.state.isLogin?logout:login}/>
                                </div>
                                <div className={'title'}>{this.state.isLogin ? '登出':'登录'}</div>
                            </Link>
                        </li>
                    </ul>
                </div>
            </Drawer>
        );
    }

    loginCallback(){
        this.setState({
            isLogin:Cache.isLogin()
        })
    }
}