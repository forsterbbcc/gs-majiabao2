import React, {Component} from 'react';
import Header from '../common/header'
import {Req} from "../../lib";
import {Cache} from "../../module";
import AlertFunction from "../../lib/AlertFunction";

//todo 图片banner


import close from '../../images/mdf-svg/close.svg';
import { Svg } from '../common';
export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            code: '',
            showImg: false,
            count: '获取验证码',
            countNumber: 60,
            imageAddress: '',
            imageCode: '',
            next: false,
            nikeName: '',
            password: '',
        }
    }

    render() {
        return (
            <div className={'register'}>
                <div className={'main'}>
                    <Header {...this.props} title={'注册'}/>
                    {
                        this.state.next ? (
                            <div className={'infoBox'}>
                                <div>
                                    {/* <div className={'title'}>姓名</div> */}
                                    <div className={'inputBox'}>
                                        <input type="text" placeholder={'取个吉利的名字吧'}
                                                value={this.state.nikeName}
                                               onChange={(e) => this.setState({nikeName: e.target.value})}
                                               maxLength={8}/>
                                    </div>
                                </div>
                                <div>
                                    {/* <div className={'title'}>登录密码</div> */}
                                    <div className={'inputBox'}>
                                        <input type="text" placeholder={'请输入登录密码'}
                                        value={this.state.password}
                                               onChange={(e) => this.setState({password: e.target.value})}
                                               maxLength={11}/>
                                    </div>
                                </div>
                                <div className={'buttonBox'}>
                                    <p className={'tips'}>6-20位数字、字母组合（特殊字符除外）</p>
                                    <div className={'submit'} onClick={() => this.submit()}>注册</div>
                                </div>

                            </div>
                        ) : (
                            <div className={'infoBox'}>
                                <div>
                                    {/* <div className={'title'}>手机号</div> */}
                                    <div className={'inputBox'}>
                                        
                                        <input type="tel" placeholder={'请输入您的手机号码'}
                                            value={this.state.username}
                                               onChange={(e) => this.setState({username: e.target.value})}
                                               maxLength={11}/>
                                    </div>
                                </div>
                                <div>
                                    {/* <div className={'title'}>短信验证码</div> */}
                                    <div className={'inputBox'}>
                                        
                                        <input type="tel" placeholder={'请输入手机短信验证码'} maxLength={4}
                                               value={this.state.code}
                                               onChange={(e) => this.setState({code: e.target.value})}/>
                                        <div className={'getCode'} onClick={() => this.show()}>{this.state.count}</div>
                                    </div>
                                    <div className={'buttonBox'}>
                                        <div className={'submit'} onClick={() => this.send()}>提交</div>
                                    </div>
                                </div>

                            </div>
                        )
                    }

                </div>
                {
                    this.state.showImg ? (
                        <div className={'cover'}>
                            <div className={'codeBox'}>
                                <p>验证码</p>
                                <div  className={'closeButton'} onClick={() => {
                                        this.setState({showImg: false})
                                    }}>
                                    <Svg path={close}/>
                                </div>
                                <div className={'code'}>
                                    <input type="text" placeholder={'请填写图片验证码'} maxLength={4}
                                           onChange={(e) => this.setState({imageCode: e.target.value})}/>
                                    <img src={this.state.imageAddress} alt=""/>
                                </div>
                                <div className={'codeButton'}>
                                    {/* <div onClick={() => {
                                        this.setState({showImg: false})
                                    }}>取消
                                    </div> */}
                                    <div className={'sure'} onClick={() => this.confirmCode()}>确定</div>
                                </div>
                            </div>
                        </div>
                    ) : (null)
                }
            </div>
        )
    }

    //todo 显示图片验证码
    show() {
        if (this.state.countNumber !== 0 && this.state.count !== '获取验证码') {
            return AlertFunction({title: '警告', msg: '验证码已经发送，请稍后再试！'})
        }

        this.setState({
            showImg: true,
            imageAddress: `/api/vf/verifyCode.jpg?_=${new Date()}`
        });

    }

    //todo 确定图片验证码
    async confirmCode() {
        if (this.state.imageCode.length !== 4) {
            AlertFunction({title: '警告', msg: '请输入正确的验证码', button: 1, confirm: () => this.show()})
        } else {
            try {
                let result = await Req({
                    url: '/api/sso/register.htm',
                    type: 'POST',
                    data: {
                        action: 'sendCode',
                        mobile: this.state.username,
                        imageCode: this.state.imageCode
                    }
                });

                AlertFunction({
                    title: '提示',
                    msg: result.errorMsg,
                    confirm: () => this.setState({showImg: false})
                });
                this.countDown()

            } catch (err) {
                AlertFunction({title: '错误', msg: err.errorMsg, confirm: () => this.show()})
            }
        }
    }

    //todo 倒计时
    countDown() {
        if (this.state.countNumber === 0) {
            this.setState({
                count: '获取验证码',
                countNumber: 60
            })
        } else {
            setTimeout(() => {
                this.setState({
                    countNumber: this.state.countNumber - 1,
                    count: `${this.state.countNumber}秒后重发`,
                });
                this.countDown()
            }, 1000)

        }
    }

    //todo 下一步
    next() {
        if (this.state.username.length === 0 || this.state.username.length !== 11) {
            return AlertFunction({title: '警告', msg: '手机号码有误，请重新输入！'})
        } else {
            this.setState({phoneCode: false})
        }
    }

    //todo 提交验证码
    send() {
        Req({
            url: '/api/sso/register.htm',
            type: 'POST',
            data: {
                action: 'verifyCode',
                verifyCode: this.state.code
            }
        }).then((data) => {
            this.setState({next: true})
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
        })
    }

    //todo 注册
    submit() {
        Req({
            url: '/api/sso/register.htm',
            type: 'POST',
            data: {
                action: 'register',
                username: this.state.nikeName,
                password: this.state.password
            }
        }).then((data) => {
            AlertFunction({title: '提示', msg: data.errorMsg, confirm: () => this.props.history.push('/')});
            Cache.status = true;
            localStorage.setItem('isLogin', true);
            localStorage.setItem('mobile', this.state.username);
            Cache.getUserInfo();
            Cache.getUserScheme();
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg});
        })
    }
}