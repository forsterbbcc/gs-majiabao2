import home from '../view/home';

import trade from '../view/trade';
import order from '../view/order';

import listInfo from '../view/position/listInfo';
import detail from '../view/info/detail';

import accountSet from '../view/account/accountSet';
import changePassword from '../view/account/changePassword';
import changePhoneNumber from '../view/account/changePhoneNumber';
import recoverPassword from '../view/account/recoverPassword';
import cs from '../view/cs';
import quotation from '../view/quotation';
import notice from '../view/notice';
import noticeInfo from '../view/notice/noticeInfo';
import topList from '../view/topList';
import newWelfare from '../view/newWelfare';
//todo 直播
import live from '../view/live';
import lives from '../view/live/live';
import finance from '../view/live/finance';
import news from '../view/live/news';
//todo 登录
import loginMain from '../view/login';
import login from '../view/login/login';
import register from '../view/register';

import position from '../view/position';
import position_settlement from '../view/position/settlement';
import positionMain from './../view/position/main'

import Favorite from './../view/account/favorite'
import Record from './../view/account/record';
import Recomment from './../view/home/recomment'

import Contact from './../view/account/contact'
import financePage from './../view/finance/index'
import event from './../view/finance/event'
import holiday from './../view/finance/holiday'
import column from './../view/live/column'
export default [
    {
        path: '/',
        exact: true,
        component: home
    },
    {
        path: '/login',
        component: login,
        // routes: [
        //     {
        //         path: '/login/account',
        //         component: login
        //     },
        //     {
        //         path: '/login/register',
        //         component: register
        //     },
        // ]
    },
    {
        path:'/column',
        component:column
    },
    {
        path: '/detail/:id',
        component: detail,
    },
    {
        path: '/Record',
        component: Record,
    },
    {
        path: '/register',
        component: register,
    },
    {
        path: '/trade',
        component: trade,
    },
    {
        path: '/order',
        component: order,
        auth: true
    },
    {
        path:'/finance',
        component:financePage,
        routes: [
            {
                path:'/finance/event',
                component:event
            },
            {
                path:'/finance/holiday',
                component:holiday
            }
        ]
    },
    {
        path: '/position',
        component: positionMain,
        auth: true,
        routes: [
            {
                path: '/position/position',
                component: position
            },
            {
                path: '/position/endBill',
                component: position_settlement
            },
        ]
    },
    {
        path: '/settlement',
        component: position_settlement,
        auth: true
    },
    {
        path: '/listInfo',
        component: listInfo,
        auth: true
    },
    //todo 账户设置
    {
        path: '/accountSet',
        component: accountSet,
    },
    {
        path: '/changePassword',
        component: changePassword,
        auth:true
    },
    {
        path: '/changePhoneNumber',
        component: changePhoneNumber
    },
    {
        path: '/recoverPassword',
        component: recoverPassword
    },
    {
        path: '/cs',
        component: cs
    },
    {
        path:'/quotation',
        component:quotation
    },
    {
        path:'/notice',
        component:notice
    },
    {
        path:'/noticeInfo',
        component:noticeInfo
    },
    {
        path:'/topList',
        component:topList
    },
    {
        path:'/newWelfare',
        component:newWelfare
    },
    {
        path:'/Favorite',
        component:Favorite
    },
    {
        path:'/recommend',
        component:Recomment
    },
    {
        path:'/contact',
        component:Contact
    },
    {
        path: '/live',
        component: live,
        routes: [
            {
                path: '/live/lives',
                component: lives
            },
            {
                path: '/live/news',
                component: news
            },
            {
                path: '/live/finance',
                component: finance
            },
        ]
    },
]