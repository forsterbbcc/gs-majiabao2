import React from 'react';
import ReactDOM from 'react-dom';
import {Alert} from "../view/common";


const AlertFunction = props => {
    const holder = document.createElement('div');
    document.body.appendChild(holder);

    const close = () => {
        document.body.removeChild(holder)
    };
    ReactDOM.render(
        <Alert
            {...props}
            close={close}
        />,
        holder
    )

};

export default AlertFunction
