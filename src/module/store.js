import {Req} from "../lib";
import rules from '../tradeRule'


export default {
    notice:{
        content:null,
        time:Date.now(),
        async get(){
            try{
                if(!this.content || Date.now() - this.time > 3600000){
                    this.content = await Req({
                        url:'/api/discover/index.htm'
                    });
                    this.time = Date.now()
                }
            }catch (err){

            }
            return new Promise((resolve,reject)=>{
                if (!!this.content) {
                    resolve(this.content);
                } else{
                    reject(null);
                }
            })
        }
    },
    rule:{
        content:rules,
        time:Date.now(),
        get() {
            return this.content
        }
    },
    banner:{
        content:[],
        time:Date.now(),
        async get(){
            try{
                if(this.content.length === 0 || Date.now() - this.time > 3600000){
                    let result = await Req({
                        url: '/api/index.htm',
                        type: "GET",
                        data: {action: 'carousel'}
                    });
                    this.time = Date.now();
                    this.content= result.carousels;
                }
            }catch (err){
                this.content = null;
            }
            return new Promise((resolve,reject)=>{
                if (!!this.content) {
                    resolve(this.content);
                } else{
                    reject(null);
                }
            })
        }
    },
    homeNotices:{
        content:[],
        time:Date.now(),
        async get(){
            try{
                if(this.content.length === 0 || Date.now() - this.time > 3600000){
                    let result = await Req({
                        url: '/api/index.htm',
                        type: "GET",
                        data: {action: 'carousel'}
                    });
                    this.time = Date.now();
                    this.content= result.notices;
                }
            }catch (err){
                this.content = null;
            }
            return new Promise((resolve,reject)=>{
                if (!!this.content) {
                    resolve(this.content);
                } else{
                    reject(null);
                }
            })
        }
    }
}